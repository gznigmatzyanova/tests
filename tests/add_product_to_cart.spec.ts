import { expect } from '@playwright/test';
import { User } from "../common/user";
import { test } from './user_types';


test.describe("Add a product to the cart", () => {
  test.beforeEach(async ({ page, type }) => {
    const user = new User(type);

    await page.goto('/');

    await page.locator('[data-test="username"]').fill(user.email);
    await page.locator('[data-test="password"]').fill(user.password);
    await page.locator('[data-test="login-button"]').click();
  });

  test('The added product is displayed correctly in the cart', async ({ page }) => {
    await page.locator('[data-test="add-to-cart-sauce-labs-fleece-jacket"]').click();

    await page.locator('#shopping_cart_container a').click();

    await expect(page.locator('div.cart_list div.cart_item')).toHaveCount(1);
    await expect(page.locator('div.cart_list div.cart_item a[id="item_5_title_link"]')).toHaveText('Sauce Labs Fleece Jacket');
    await expect(page.locator('div.cart_list div.cart_item div.item_pricebar div.inventory_item_price')).toHaveText('$49.99');

    // как будто по заданию нужно было завершить покупку
    // но кмк мы тут проверяли именно корректность помещенного айтема в корзину, а не саму покупку
    // await page.locator('[data-test="checkout"]').click();
    // await page.locator('[data-test="firstName"]').fill('John');
    // await page.locator('[data-test="lastName"]').fill('Doe');
    // await page.locator('[data-test="postalCode"]').fill('90210');
    // await page.locator('[data-test="continue"]').click();

    // await page.locator('[data-test="finish"]').click();
  });

  test('The added product is taken into account in the cart icon', async ({ page }) => {
    await page.locator('[data-test="add-to-cart-sauce-labs-bike-light"]').click();
    await page.locator('#shopping_cart_container a').click();

    await expect(page.locator('a.shopping_cart_link')).toHaveText("1");

    // я бы разделила на два теста, один на увеличение счетчика, второй на уменьшение
    // чтобы тест завершался expect-лм завершался 
    await page.locator('[data-test="remove-sauce-labs-bike-light"]').click();

    await expect(page.locator('a.shopping_cart_link')).not.toHaveText("1");
  });

  test.afterEach(async ({ page }) => {
    await page.getByRole('button', { name: 'Open Menu' }).click();
    await page.getByRole('link', { name: 'Logout' }).click();
  });
});
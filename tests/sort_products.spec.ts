import { expect } from '@playwright/test';
import { User } from "../common/user";
import { test } from './user_types';

test.describe("Sort products", () => {
  test.beforeEach(async ({ page, type }) => {
    const user = new User(type);

    await page.goto('/');

    await page.locator('[data-test="username"]').fill(user.email);
    await page.locator('[data-test="password"]').fill(user.password);
    await page.locator('[data-test="login-button"]').click();
  });

  test('Products are correctly sorted in ascending order', async ({ page }) => {
    await page.locator('[data-test="product_sort_container"]').selectOption('az');

    let products = await page.locator('div.inventory_list div.inventory_item').all();
    let productsTitle: string[] = [];

    for (let i = 0; i < products.length; i++) {
      let productTitle = await products[i].locator('div.inventory_item_name').textContent();
      const productTitleValue = productTitle || "";

      productsTitle.push(productTitleValue);
    }

    const productsTitleExpected = productsTitle.concat([]).sort();

    await expect(productsTitle).toStrictEqual(productsTitleExpected);
  });

  test('Products are correctly sorted in descending order', async ({ page }) => {
    await page.locator('[data-test="product_sort_container"]').selectOption('za');

    let products = await page.locator('div.inventory_list div.inventory_item').all();
    let productsTitle: string[] = [];

    for (let i = 0; i < products.length; i++) {
      let productTitle = await products[i].locator('div.inventory_item_name').textContent();
      const productTitleValue = productTitle || "";

      productsTitle.push(productTitleValue);
    }

    const productsTitleExpected = productsTitle.concat([]).sort().reverse();

    await expect(productsTitle).toStrictEqual(productsTitleExpected);
  });

  test.afterEach(async ({ page }) => {
    await page.getByRole('button', { name: 'Open Menu' }).click();
    await page.getByRole('link', { name: 'Logout' }).click();
  });
});
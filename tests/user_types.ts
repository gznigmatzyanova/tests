import { test as base } from '@playwright/test';

export type TestOptions = {
    type: string;
};

export const test = base.extend<TestOptions>({
    type: ['standard_user', { option: true }],
});
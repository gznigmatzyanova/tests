import { defineConfig, devices } from '@playwright/test';
import type { TestOptions } from './tests/user_types';

export default defineConfig<TestOptions>({
  testDir: './tests',
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: [['html', { open: 'always' }]], 
  use: {
    trace: 'on-first-retry',
    baseURL: 'https://www.saucedemo.com',
  },

  projects: [
    {
      name: 'standard_user',
      use: { type: 'standard_user' },
    },
    {
      name: 'performance_glitch_user',
      use: { type: 'performance_glitch_user' },
    }
  ],
});

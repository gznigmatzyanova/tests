# Test project

## Run

```
npx playwright test --reporter=line,allure-playwright
```

## See html report

```
npx playwright show-report
```
## See Allure report

```
allure generate ./allure-results --clean
allure open ./allure-report   
```

### Main branch

Оценка до начала работ: 2-3 часа  
Реальная оценка: 3.5 часа  

### TBD branch
Что хотелось бы доделать, но не успела:

* ~~Добавить кастомизацию выбора пользователя, по флагу при запуске теста~~ 
* Нашла раздел про кастомизацию тестов в конфиге, наверное имелось в виду именно это - done в **main** branch (случайно залила туда, но решила не мучиться с откатом) (+ 30 минут)
* Имплементировать PageObject - done в **tbd** branch (+ 3 часа)
* Вынести шаги авторизации и логаута в класс User (реализовать методы: юай автризации, апи+куки авторизации)